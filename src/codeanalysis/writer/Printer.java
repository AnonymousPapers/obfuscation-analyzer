package org.anonymous.codeanalysis.writer;

public interface Printer {
    public void print() throws Exception;
}